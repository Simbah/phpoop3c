<?php
  // memanggil file koneksi.php untuk melakukan koneksi database
  include 'koneksi.php';
?>

<!DOCTYPE html>
<html>
  <head>
    <style>
      table{
        width: 1350px;
        margin: auto;
      }
      h1{
        text-align: center;
	
      }
    </style>
  </head>
  <body>
    <h1>Tabel Data Pegawai</h1>
    <center><a href="input.php"><b>Input Data Pegawai &Gt; </b></a></center>
    <br/>
    <table border="1" >
      <tr>
        <th>No</th>
        <th>NIK</th>
        <th>Nama Lengkap</th>
		 <th>Gelar Depan</th>
		  <th>Gelar Belakang</th>
		   <th>Identitas</th>
		    <th>No Id</th>
			 <th>Tpt Lahir</th>
			  <th>Tgl Lahir</th>
			  
        <th>Jenis Kelamin</th>
        <th>Telepon</th> 
        <th>HP</th>
        <th>Email</th>
		 <th>Website</th>
      </tr>
      <?php
      // jalankan query untuk menampilkan semua data diurutkan berdasarkan nik
      $query = "SELECT * FROM pegawai ORDER BY nik ASC";
      $result = mysqli_query($link, $query);
      //mengecek apakah ada error ketika menjalankan query
      if(!$result){
        die ("Query Error: ".mysqli_errno($link).
           " - ".mysqli_error($link));
      }

      //buat perulangan untuk element tabel dari data pegawai
      $no = 1; //variabel untuk membuat nomor urut
      // hasil query akan disimpan dalam variabel $data dalam bentuk array
      // kemudian dicetak dengan perulangan while
      while($data = mysqli_fetch_assoc($result))
      {
        // mencetak / menampilkan data
        echo "<tr>";
        echo "<td>$no</td>"; //menampilkan no urut
        echo "<td>$data[nik]</td>"; //menampilkan data nik
        echo "<td>$data[nama_lengkap]</td>"; //menampilkan data nama
		echo "<td>$data[glar_dpan]</td>";
		echo "<td>$data[glar_blkg]</td>";
		echo "<td>$data[identitas]</td>";
		echo "<td>$data[no_id]</td>";
		echo "<td>$data[tpt_lhr]</td>";
		echo "<td>$data[tgl_lhr]</td>";
        echo "<td>$data[jenkel]</td>"; //menampilkan data jenis kelamin
        echo "<td>$data[telepon]</td>"; //menampilkan data no telepon
		echo "<td>$data[hp]</td>";
		echo "<td>$data[email]</td>";
		echo "<td>$data[website]</td>";
       
        // membuat link untuk mengedit dan menghapus data
        echo '<td>
          <a href="#">Edit</a> /
          <a href="#";
      		  onclick="return confirm(\'Anda yakin akan menghapus data?\')">Hapus</a>
        </td>';
        echo "</tr>";
        $no++; // menambah nilai nomor urut
      }
      ?>
    </table>
  </body>
</html>