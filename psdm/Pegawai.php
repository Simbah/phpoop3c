<?php
    include_once "Class/Pegawai.class.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <?php
        $data = new Pegawai();
        $result = $data->tampil();
        $hasil = $result->fetchAll();
    ?>
    <a href="Pegawai.tambah.php">Tambah</a>
    <table border="1">
        <thead>
            <tr>
                <th>No</th>
                <th>NIK</th>
                <th>Nama Lengkap</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php $no = 1;?>
            <?php foreach($hasil as $row):?>
                <tr>
                    <td><?php echo $no++;?></td>
                    <td><?php echo $row['nik'];?></td>
                    <td><?php echo $row['nama_lengkap'];?></td>
                    <td>Edit | Hapus</td>
                </tr>
            <?php endforeach;?>
        </tbody>
    </table>
</body>
</html>